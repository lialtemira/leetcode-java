# LeetCode OJ

This repository implements solutions to the [LeetCode OJ](https://leetcode.com) problems using Java.

## Requirements
* [Java JDK 8+](http://www.oracle.com/technetwork/java/javase/downloads/index.html)

## Quickstart
Run the following in the `src/main` folder:

```
javac [mainjava].java
java [mainjava]
```
